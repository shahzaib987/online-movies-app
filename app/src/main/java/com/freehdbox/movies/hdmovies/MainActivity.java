package com.freehdbox.movies.hdmovies;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends AppCompatActivity {

    WebView webview;
    AdView mAdView;
    InterstitialAd mInterstitialAd;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the InterstitialAd and set the adUnitId.
        mInterstitialAd = new InterstitialAd(this);
        // Defined in res/values/strings.xml
        mInterstitialAd.setAdUnitId(getString(R.string.test_ad_unit_interstitial));

        AdRequest adRequest1 = new AdRequest.Builder().build();

        mInterstitialAd.loadAd(adRequest1);

        mInterstitialAd.setAdListener(new AdListener(){
            public void onAdLoaded(){
                mInterstitialAd.show();
            }
        });

        mAdView = (AdView) findViewById(R.id.ad_view);

        // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
        //  AdRequest adRequest = new AdRequest.Builder().build();
        /*AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("98506F81887DC89001B2C6CA37BC8114")
                .build();*/
        // Start loading the ad in the background.
        mAdView.loadAd(adRequest1);

        webview = (WebView) findViewById(R.id.webView);

        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webview.setWebViewClient(new WebViewClient());
        webview.loadUrl("https://yesmovies.to");
    }

    @Override
    public void onBackPressed() {

        if (webview.canGoBack()) {
            webview.goBack();
            // mInterstitialAd.show();
        } else {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                builder = new AlertDialog.Builder(MainActivity.this);
                mInterstitialAd.show();
            }
            else
            {
                builder = new AlertDialog.Builder(MainActivity.this, AlertDialog.BUTTON_NEUTRAL);
            }
            builder.setTitle("Thank You");
            builder.setMessage("Thank You For Using Our Application Please Give Us Your Suggestions and Feedback ");
            builder.setNegativeButton("RATE US",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog,
                                            int which)
                        {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=ADD YOUR APPS PACKAGE NAME")); // Add package name of your application
                            startActivity(intent);
                            Toast.makeText(MainActivity.this, "Thank you for your Rating",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
            builder.setPositiveButton("QUIT",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog,
                                            int which)
                        {
                            mInterstitialAd.show();
                            finish();

                        }
                    });

            builder.show();
        }


        // super.onBackPressed();
        }




}
